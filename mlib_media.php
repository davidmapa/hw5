<!doctype html>
<?php
require('mlib_functions.php');
html_head("Add Media");
require('mlib_header.php');
require('mlib_sidebar.php');

# Code for your web page follows.
if (!isset($_POST['submit']))
{
?>
  <!-- Display a form to capture information -->
  <h2>Add Media</h2>
  <form action="mlib_media.php" method="post">
    <table border="0">
      <tr bgcolor="#cccccc">
        <td width="100"><b>Field</b></td>
        <td width="300"><b>Value</b></td>
      </tr>
      <tr>
        <td>Title</td>
        <td align="left"><input type="text" name="mediatitle" size="50" maxlength="50"></td>
      </tr>
	  <tr>
        <td>Author</td>
        <td align="left"><input type="text" name="mediaauthor" size="50" maxlength="50"></td>
      </tr>
	  <tr>
        <td>Description</td>
        <td align="left"><input type="text" name="mediadescription" size="80" maxlength="100"></td>
      </tr>
	  <tr>
        <td>Type</td>
        <td align="left"><input type="text" name="mediatype" size="50" maxlength="50"></td>
      </tr>
      <tr>
        <td colspan="2" align="right"><input type="submit" name="submit" value="Submit"></td>
      </tr>
    </table>
  </form>
<?php
} else {
  # Process the information from the form displayed
  $mediatitle = $_POST['mediatitle'];
  $mediaauthor = $_POST['mediaauthor'];
  $mediadescription = $_POST['mediadescription'];
  $mediatype = $_POST['mediatype'];
  
  print "<h2>Media Added</h2>";
  print "<table border=1 cellpadding=5 >";
  print "<tr>";
  print "<td><b>Title</b></td>";
  print "<td><b>Author</b></td>";
  print "<td><b>Description</b></td>";
  print "<td><b>Type</b></td>";
  print "</tr>";
  print "<tr>";
  print "<td>".$mediatitle."</td>";
  print "<td>".$mediaauthor."</td>";
  print "<td>".$mediadescription."</td>";
  print "<td>".$mediatype."</td>";
  print "</tr>";
  print "</table>";
}
require('mlib_footer.php');
?>
